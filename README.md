# VScript -- A Bash Library Tool

## About
VScript is a tool for making bash scripts more powerful. Because bash is available on everything from a jailbroken iPhone to an enterprise server it makes sense to utilise its portability. So I created VScript to accomplish this.

## What Can It Do?
So far I have made libraries for:
Error Management -- Handling Errors In A Way That Makes Testing Easier

And I am working on:
Windowing support -- something to unify a basic text, ncurses, and zenity interface.
Networking -- allow you to write simple services in bash.

## Download
To Get VScript you can either:
Grab a Release that contains precompiled libraries
Get the latest source/release source and compile it yourself, with VSC.




